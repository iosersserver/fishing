import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import net.seliba.fishing.utils.random.IntegerRange;
import org.junit.jupiter.api.Test;

public class IntegerRangeTests {

  @Test
  public void testInsideValues() {
    var range1 = new IntegerRange(0, 5);
    var range2 = new IntegerRange(-50, -20);
    var range3 = new IntegerRange(5000, 6000);

    for (int i = 0; i <= 5; i++) {
      assertTrue(range1.isValueInside(i));
    }

    for (int i = -50; i <= -20; i++) {
      assertTrue(range2.isValueInside(i));
    }

    for (int i = 5000; i <= 6000; i++) {
      assertTrue(range3.isValueInside(i));
    }
  }

  @Test
  public void testOutsideValues() {
    var range1 = new IntegerRange(0, 5);
    var range2 = new IntegerRange(-50, -20);
    var range3 = new IntegerRange(5000, 6000);

    for (int i = -500; i < 0; i++) {
      assertFalse(range1.isValueInside(i));
      assertFalse(range3.isValueInside(i));    }

    for (int i = -500; i < -50; i++) {
      assertFalse(range1.isValueInside(i));
      assertFalse(range2.isValueInside(i));
      assertFalse(range3.isValueInside(i));    }

    for (int i = 4000; i < 5000; i++) {
      assertFalse(range1.isValueInside(i));
      assertFalse(range2.isValueInside(i));
      assertFalse(range3.isValueInside(i));
    }

    for (int i = 6; i < 500; i++) {
      assertFalse(range1.isValueInside(i));
      assertFalse(range2.isValueInside(i));
      assertFalse(range3.isValueInside(i));    }

    for (int i = -19; i < 500; i++) {
      assertFalse(range2.isValueInside(i));
      assertFalse(range3.isValueInside(i));    }

    for (int i = 6001; i < 7000; i++) {
      assertFalse(range1.isValueInside(i));
      assertFalse(range2.isValueInside(i));
      assertFalse(range3.isValueInside(i));    }
  }

  @Test
  public void testBoundaryValidation() {
    assertThrows(IllegalArgumentException.class, () -> new IntegerRange(5, 1));
    assertThrows(IllegalArgumentException.class, () -> new IntegerRange(-1, -2));
    assertThrows(IllegalArgumentException.class, () -> new IntegerRange(1000, -1000));
    assertDoesNotThrow(() -> new IntegerRange(0, 1));
    assertDoesNotThrow(() -> new IntegerRange(-100, 100));
    assertDoesNotThrow(() -> new IntegerRange(-5000, -4999));
    assertDoesNotThrow(() -> new IntegerRange(1, 1));
    assertDoesNotThrow(() -> new IntegerRange(-1000, -1000));
  }

}
