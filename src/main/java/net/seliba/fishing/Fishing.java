package net.seliba.fishing;

import net.seliba.fishing.listener.EntitySpawnListener;
import net.seliba.fishing.listener.PlayerFishListener;
import net.seliba.fishing.loot.LootGenerator;
import net.seliba.fishing.scheduler.RodParticleScheduler;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * The main class used by the plugin.
 */
public class Fishing extends JavaPlugin {

  /**
   * Method which gets called when the plugin is loading.
   */
  @Override
  public void onEnable() {
    startScheduler();
    registerListener();
    LootGenerator.init();
    getLogger().info("Das Plugin wurde erfolgreich geladen!");
  }

  /**
   * Initializes the {@link RodParticleScheduler} do display a particle trail behind fishing hooks.
   */
  private void startScheduler() {
    RodParticleScheduler.start();
  }

  /**
   * Register all event listeners required by this plugin.
   */
  private void registerListener() {
    var pluginManager = Bukkit.getPluginManager();

    pluginManager.registerEvents(
      new EntitySpawnListener(),
      this
    );
    pluginManager.registerEvents(
      new PlayerFishListener(),
      this
    );
  }

}
