package net.seliba.fishing.scheduler;

import java.util.ArrayList;
import java.util.List;
import net.seliba.fishing.Fishing;
import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.Particle.DustOptions;
import org.bukkit.entity.FishHook;

/**
 * Class which shows particles when a rod is being cast but didn't land in water / is still in the
 * air. Only a cosmetic feature.
 */
public class RodParticleScheduler {

  private static final List<FishHook> HOOKS = new ArrayList<>();
  private static final DustOptions GREEN_DUST = new DustOptions(
    Color.GREEN,
    1
  );
  private static final DustOptions RED_DUST = new DustOptions(
    Color.RED,
    1
  );

  /**
   * Initializes the schedulers.
   */
  public static void start() {
    startHookParticleRemover();
    startFistColorScheduler();
    startSecondColorScheduler();
  }

  /**
   * Registers a fishing hook which should have a particle trail.
   *
   * @param hook The cast fishing rod.
   */
  public static void registerHook(FishHook hook) {
    HOOKS.add(hook);
  }

  /**
   * Removes hooks that are no longer applicable for a particle trail.
   */
  private static void startHookParticleRemover() {
    Bukkit.getScheduler()
      .scheduleSyncRepeatingTask(
        Fishing.getPlugin(Fishing.class),
        () -> HOOKS.removeIf(hook -> hook == null || hook.isDead() || hook.getLocation()
          .getBlock()
          .getType() == Material.WATER),
        0L,
        10L
      );
  }

  /**
   * Displays a green trail behind the fishing hooks.
   */
  private static void startFistColorScheduler() {
    Bukkit.getScheduler()
      .scheduleSyncRepeatingTask(
        Fishing.getPlugin(Fishing.class),
        () -> {
          for (FishHook hook : HOOKS) {
            hook.getWorld()
              .spawnParticle(
                Particle.REDSTONE,
                hook.getLocation(),
                10,
                GREEN_DUST
              );
          }
        },
        0L,
        6L
      );
  }

  /**
   * Displays a red trail behind the fishing hooks.
   */
  private static void startSecondColorScheduler() {
    Bukkit.getScheduler()
      .scheduleSyncRepeatingTask(
        Fishing.getPlugin(Fishing.class),
        () -> {
          for (FishHook hook : HOOKS) {
            hook.getWorld()
              .spawnParticle(
                Particle.REDSTONE,
                hook.getLocation(),
                10,
                RED_DUST
              );
          }
        },
        4L,
        6L
      );
  }

}
