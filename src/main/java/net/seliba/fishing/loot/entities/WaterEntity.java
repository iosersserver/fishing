package net.seliba.fishing.loot.entities;

import java.util.function.Consumer;
import net.seliba.fishing.loot.LootRarity;
import net.seliba.fishing.utils.random.RandomObject;
import org.bukkit.entity.Ageable;
import org.bukkit.entity.Breedable;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Zombie;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public enum WaterEntity implements RandomObject {

  // TODO (Ideen): LootTable, EntityEquipmentTable
  SQUID(
    "Tintenfisch",
    LootRarity.COMMON,
    EntityType.SQUID,
    20,
    null
  ), SALMON(
    "Lachs",
    LootRarity.COMMON,
    EntityType.SALMON,
    10,
    null
  ), COD(
    "Dorsch",
    LootRarity.COMMON,
    EntityType.COD,
    10,
    null
  ), OLD_SQUID(
    "Alter Tintenfisch",
    LootRarity.RARE,
    EntityType.SQUID,
    50,
    null
  ), CATFISH(
    "Wels",
    LootRarity.RARE,
    EntityType.COD,
    10,
    null
  ), DROWNED(
    "Ertrunkener",
    LootRarity.RARE,
    EntityType.DROWNED,
    20,
    null
  ), TROPICAL_FISH(
    "Tropischer Fisch",
    LootRarity.EPIC,
    EntityType.TROPICAL_FISH,
    20,
    null
  ), DOLPHIN(
    "Delphin",
    LootRarity.EPIC,
    EntityType.DOLPHIN,
    20,
    null
  ), OLD_DROWNED(
    "Alter Ertrunkener",
    LootRarity.EPIC,
    EntityType.DROWNED,
    50,
    null
  ), GUARDIAN(
    "Wächter",
    LootRarity.EPIC,
    EntityType.GUARDIAN,
    30,
    null
  ), FLYING_FISH(
    "Fliegender Fisch",
    LootRarity.LEGENDARY,
    EntityType.PUFFERFISH,
    100,
    entity -> {
      entity.addPotionEffect(new PotionEffect(
        PotionEffectType.LEVITATION,
        10 * 20,
        1
      ));
    }
  ), BABY_DROWNED(
    "Süßer Baby-Ertrunkener",
    LootRarity.LEGENDARY,
    EntityType.DROWNED,
    80,
    entity -> {
      var zombie = (Zombie) entity;
      zombie.setBaby();
      ((Breedable) zombie).setAgeLock(true);
      entity.addPotionEffect(new PotionEffect(PotionEffectType.SPEED,
        100000 * 20,
        5,
        false,
        false
      ));
    }
  ), OLD_GUARDIAN(
    "Großer Wächter",
    LootRarity.LEGENDARY,
    EntityType.ELDER_GUARDIAN,
    100,
    entity -> entity.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION,
      5 * 20,
      3,
      false,
      false
    ))
  );

  public final String name;
  public final LootRarity lootRarity;
  public final EntityType entityType;
  public final int hp;
  public final Consumer<LivingEntity> additionalModifiers;

  WaterEntity(String name, LootRarity lootRarity, EntityType entityType, int hp, Consumer<LivingEntity> additionalModifiers) {
    this.name = name;
    this.lootRarity = lootRarity;
    this.entityType = entityType;
    this.hp = hp;
    this.additionalModifiers = additionalModifiers;
  }

  @Override
  public int getPossibility() {
    return lootRarity.getPossibility();
  }

}
