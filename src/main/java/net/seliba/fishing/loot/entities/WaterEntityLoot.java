package net.seliba.fishing.loot.entities;

import java.util.Arrays;
import java.util.function.Consumer;
import net.seliba.fishing.Fishing;
import net.seliba.fishing.loot.Loot;
import net.seliba.fishing.loot.LootGenerator;
import net.seliba.fishing.utils.random.RandomList;
import net.seliba.fishing.utils.vector.VectorUtil;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Mob;
import org.bukkit.entity.Player;

public class WaterEntityLoot extends Loot {

  @Override
  public int getPossibility() {
    return 15;
  }

  @Override
  public void executeRandomLootGeneration(Location fishHookLocation, Player player) {
    Bukkit.broadcastMessage("Water entity!");

    var randomList = new RandomList<WaterEntity>();
    Arrays.stream(WaterEntity.values())
      .forEach(randomList::add);
    var selectedMob = randomList.getRandomEntry();

    LivingEntity spawnedEntity = (LivingEntity) fishHookLocation.getWorld()
      .spawnEntity(fishHookLocation, selectedMob.entityType);
    spawnedEntity.getAttribute(Attribute.GENERIC_MAX_HEALTH)
      .setBaseValue(selectedMob.hp);
    spawnedEntity.setHealth(selectedMob.hp);
    spawnedEntity.setCustomNameVisible(true);
    spawnedEntity.setCustomName(selectedMob.lootRarity.color + selectedMob.name);
    ((Mob) spawnedEntity).setTarget(player);

    var additionalModifiers = selectedMob.additionalModifiers;
    if (additionalModifiers != null) {
      additionalModifiers.accept(spawnedEntity);
    }

    spawnedEntity.setVelocity(VectorUtil.getVectorBetweenLocations(
      fishHookLocation,
      player.getLocation()
        .add(0, 2, 0)
    )
      .multiply(0.5D));
  }

}
