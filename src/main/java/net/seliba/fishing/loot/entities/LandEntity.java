package net.seliba.fishing.loot.entities;

import de.tr7zw.changeme.nbtapi.NBTEntity;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import net.md_5.bungee.api.chat.TextComponent;
import net.seliba.fishing.Fishing;
import net.seliba.fishing.loot.LootRarity;
import net.seliba.fishing.utils.random.RandomNumberGenerator;
import net.seliba.fishing.utils.random.RandomObject;
import net.seliba.fishing.utils.vector.VectorUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.NamespacedKey;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.Bee;
import org.bukkit.entity.Breedable;
import org.bukkit.entity.Cat;
import org.bukkit.entity.Cat.Type;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.IronGolem;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Llama;
import org.bukkit.entity.LlamaSpit;
import org.bukkit.entity.Player;
import org.bukkit.entity.Rabbit;
import org.bukkit.entity.Wolf;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public enum LandEntity implements RandomObject {

  SPIDER(
    "Spinne",
    LootRarity.COMMON,
    EntityType.SPIDER,
    20,
    null
  ), CHICKEN(
    "Huhn",
    LootRarity.COMMON,
    EntityType.CHICKEN,
    10,
    null
  ), COW(
    "Kuh",
    LootRarity.COMMON,
    EntityType.COW,
    20,
    null
  ), SKELETON(
    "Skelett",
    LootRarity.RARE,
    EntityType.SKELETON,
    30,
    null
  ), CAVE_SPIDER(
    "Höhlenspinne",
    LootRarity.RARE,
    EntityType.CAVE_SPIDER,
    50,
    (entity, player) -> {
      entity.addPotionEffect(new PotionEffect(
        PotionEffectType.SPEED,
        10 * 20,
        3
      ));
    }
  ), RABBIT(
    "Hase",
    LootRarity.RARE,
    EntityType.RABBIT,
    10,
    ((entity, player) -> {
      var rabbit = (Rabbit) entity;
      if (RandomNumberGenerator.hasHappened(5)) {
        rabbit.setCustomName("Toast");
      }
    })
  ), WOLF(
    "Wolf",
    LootRarity.RARE,
    EntityType.WOLF,
    20,
    ((entity, player) -> {
      ((Wolf) entity).setAngry(true);
    })
  ), HORSE(
    "Pferd",
    LootRarity.RARE,
    EntityType.HORSE,
    40,
    null
  ), KILLER_BUNNY(
    "Killerhase",
    LootRarity.RARE,
    EntityType.RABBIT,
    40,
    (entity, player) -> {
      var rabbit = (Rabbit) entity;
      rabbit.setRabbitType(Rabbit.Type.THE_KILLER_BUNNY);
      if (RandomNumberGenerator.hasHappened(10)) {
        rabbit.setBaby();
        rabbit.setAgeLock(true);
      }
    }
  ), BEE(
    "Biene",
    LootRarity.EPIC,
    EntityType.BEE,
    40,
    null
  ), BABY_DOG(
    "Welpe",
    LootRarity.EPIC,
    EntityType.WOLF,
    100,
    (entity, player) -> {
      var wolf = (Wolf) entity;
      wolf.setBaby();
      wolf.setAgeLock(true);
      wolf.setTamed(true);
      wolf.setOwner(player);
      wolf.setCollarColor(DyeColor.values()[ThreadLocalRandom.current()
        .nextInt(DyeColor.values().length)]);
    }
  ), IRON_GOLEM(
    "Eisengolem",
    LootRarity.EPIC,
    EntityType.IRON_GOLEM,
    75,
    (entity, player) -> {
      entity.addPotionEffect(new PotionEffect(
        PotionEffectType.INCREASE_DAMAGE,
        5 * 20,
        3
      ));
      ((IronGolem) entity).setTarget(player);
    }
  ), JOHNNY_VINDICATOR(
    "Johnny",
    LootRarity.EPIC,
    EntityType.VINDICATOR,
    50,
    (entity, player) -> entity.getPersistentDataContainer()
      .set(
        NamespacedKey.minecraft("johnny"),
        PersistentDataType.BYTE,
        (byte) 1
      )
  ), BABY_POLAR_BEAR(
    "Baby-Eisbär",
    LootRarity.LEGENDARY,
    EntityType.POLAR_BEAR,
    50,
    (entity, player) -> {
      var ageable = (Breedable) entity;
      ageable.setBaby();
      ageable.setAgeLock(true);
    }
  ), HAMSTER(
    "Hamster",
    LootRarity.LEGENDARY,
    EntityType.CAT,
    100,
    (entity, player) -> {
      var cat = (Cat) entity;
      cat.setBaby();
      cat.setAgeLock(true);
      cat.setCatType(Type.RED);
      cat.setTamed(true);
      cat.setOwner(player);
      cat.getAttribute(Attribute.GENERIC_MOVEMENT_SPEED)
        .setBaseValue(2.5D);
    }
  ), BEE_QUEEN(
    "Bienenkönigin",
    LootRarity.LEGENDARY,
    EntityType.BEE,
    150,
    (entity, player) -> {
      var bee = (Bee) entity;
      bee.setAnger(1);
      bee.setTarget(player);
      bee.getAttribute(Attribute.GENERIC_FLYING_SPEED)
        .setBaseValue(3D);
      bee.getAttribute(Attribute.GENERIC_FOLLOW_RANGE)
        .setBaseValue(200D);
      List<Bee> otherBees = new ArrayList<>();
      for (int i = 0; i < 5; i++) {
        Bukkit.getScheduler()
          .runTaskLater(
            Fishing.getPlugin(Fishing.class),
            () -> {
              Bee otherBee = (Bee) bee.getLocation()
                .getWorld()
                .spawnEntity(
                  bee.getLocation(),
                  EntityType.BEE
                );
              otherBees.add(bee);
              otherBee.setCustomNameVisible(true);
              otherBee.setCustomName("§cWächterbiene");
              otherBee.getAttribute(Attribute.GENERIC_MAX_HEALTH)
                .setBaseValue(30D);
              otherBee.setHealth(30);
              otherBee.setAnger(1);
              otherBee.setTarget(player);
            },
            i * 50L
          );
      }
      int taskId = Bukkit.getScheduler()
        .scheduleSyncRepeatingTask(
          Fishing.getPlugin(Fishing.class),
          () -> {
            Consumer<Bee> resetBee = (Bee targetBee) -> {
              targetBee.setHasStung(false);
              if (player != null) {
                targetBee.setTarget(player);
              }
            };
            if (!bee.isDead()) {
              resetBee.accept(bee);
              otherBees.stream()
                .filter(bee1 -> !bee1.isDead())
                .forEach(resetBee);
            } else {
              otherBees.stream()
                .filter(bee1 -> !bee1.isDead())
                .forEach(Bee::remove);
            }
          },
          20L,
          20L
        );
      Bukkit.getScheduler()
        .runTaskLater(
          Fishing.getPlugin(Fishing.class),
          () -> {
            Bukkit.getScheduler()
              .cancelTask(taskId);
            otherBees.stream()
              .filter(bee1 -> !bee1.isDead())
              .forEach(Bee::remove);
          },
          5L * 60L * 20L
        );
    }
  ), BETTER_KILLER_RABBIT(
    "Niedlicher Hase",
    LootRarity.LEGENDARY,
    EntityType.RABBIT,
    100,
    (entity, player) -> {
      var rabbit = (Rabbit) entity;
      rabbit.setRabbitType(Rabbit.Type.THE_KILLER_BUNNY);
      rabbit.addPotionEffect(new PotionEffect(
        PotionEffectType.JUMP,
        100000 * 20,
        5,
        false,
        false
      ));
      rabbit.getAttribute(Attribute.GENERIC_MOVEMENT_SPEED)
        .setBaseValue(3D);
    }
  ), JEB_SHEEP(
    "Regenbogenschaf",
    LootRarity.LEGENDARY,
    EntityType.SHEEP,
    50,
    (entity, player) -> {
      NBTEntity nbtEntity = new NBTEntity(entity);
      nbtEntity.setString("CustomName", "{\"text\":\"jeb_\"}");
    }
  ), KARL_LLAMA(
    "Karl",
    LootRarity.LEGENDARY,
    EntityType.LLAMA,
    100,
    (entity, player) -> {
      var llama = (Llama) entity;
      llama.setStrength(5);
      llama.setTarget(player);
      int taskId = Bukkit.getScheduler()
        .scheduleSyncRepeatingTask(
          Fishing.getPlugin(Fishing.class),
          () -> {
            if (!llama.isDead()) {
              llama.launchProjectile(
                LlamaSpit.class,
                VectorUtil.getVectorBetweenLocations(
                  llama.getLocation(),
                  player.getLocation()
                )
                  .multiply(10)
              );
            }
          },
          0L,
          5L
        );
      Bukkit.getScheduler()
        .runTaskLater(
          Fishing.getPlugin(Fishing.class),
          () -> Bukkit.getScheduler()
            .cancelTask(taskId),
          2L * 60L * 20L
        );
    }
  );

  public String name;
  public LootRarity lootRarity;
  public EntityType entityType;
  public int hp;
  public BiConsumer<LivingEntity, Player> additionalModifiers;

  LandEntity(String name, LootRarity lootRarity, EntityType entityType, int hp, BiConsumer<LivingEntity, Player> additionalModifiers) {
    this.name = name;
    this.lootRarity = lootRarity;
    this.entityType = entityType;
    this.hp = hp;
    this.additionalModifiers = additionalModifiers;
  }

  @Override
  public int getPossibility() {
    return lootRarity.getPossibility();
  }

}
