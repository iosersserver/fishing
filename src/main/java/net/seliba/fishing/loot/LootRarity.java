package net.seliba.fishing.loot;

import net.seliba.fishing.utils.random.RandomObject;
import org.bukkit.ChatColor;

/**
 * Represents the rarity of a possible loot which can be fished.
 */
public enum LootRarity implements RandomObject {

  LEGENDARY(
    true,
    "§kXXX §6§lLEGENDÄRER FANG §r§kXXX",
    ChatColor.GOLD,
    1
  ), EPIC(
    true,
    "§5Epischer Fang!",
    ChatColor.DARK_PURPLE,
    7
  ), RARE(
    true,
    "§9Seltener Fang!",
    ChatColor.DARK_BLUE,
    32
  ), COMMON(
    false,
    "(naja reden wir nicht drüber)",
    ChatColor.GRAY,
    160
  );

  public final boolean shouldBeAnnounced;
  public final String prefix;
  public final ChatColor color;
  private final int possibility;

  /**
   * Represents the rarity of loot.
   *
   * @param shouldBeAnnounced Whether or not the player should be notified about the catch.
   * @param prefix The prefix of the message.
   * @param color The color of the rarity, e.g. for colored mob names.
   * @param possibility The possibility of this rarity being selected.
   */
  LootRarity(boolean shouldBeAnnounced, String prefix, ChatColor color, int possibility) {
    this.shouldBeAnnounced = shouldBeAnnounced;
    this.prefix = prefix;
    this.color = color;
    this.possibility = possibility;
  }

  /**
   * The chance of this rarity being chosen. The actual possibility depends on the other elements in
   * the {@link net.seliba.fishing.utils.random.RandomList}.
   *
   * @return The chance of this rarity being chosen.
   */
  @Override
  public int getPossibility() {
    return possibility;
  }

}
