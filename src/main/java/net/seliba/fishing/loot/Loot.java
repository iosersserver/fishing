package net.seliba.fishing.loot;

import net.seliba.fishing.utils.random.RandomObject;
import org.bukkit.Location;
import org.bukkit.entity.Player;

/**
 * Abstraction of loot which can be randomly selected after reeling a fishing rod.
 */
public abstract class Loot implements RandomObject {

  /**
   * Randomly selects loot and spawns it in the world.
   *
   * @param fishHookLocation The location of the fish hook which was reeled in.
   * @param player The player who cast his rod.
   */
  public abstract void executeRandomLootGeneration(Location fishHookLocation, Player player);

}
