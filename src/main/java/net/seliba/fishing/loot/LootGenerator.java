package net.seliba.fishing.loot;

import net.seliba.fishing.loot.entities.LandEntityLoot;
import net.seliba.fishing.loot.entities.WaterEntityLoot;
import net.seliba.fishing.loot.items.ItemLoot;
import net.seliba.fishing.utils.random.RandomList;

public class LootGenerator {

  private static final RandomList<Loot> RANDOM_LOOT = new RandomList<>();

  public static void init() {
    if (!RANDOM_LOOT.isEmpty()) {
      return;
    }

    RANDOM_LOOT.add(new LandEntityLoot());
    RANDOM_LOOT.add(new WaterEntityLoot());
    RANDOM_LOOT.add(new ItemLoot());
  }

  public static Loot generate() {
    return RANDOM_LOOT.getRandomEntry();
  }

}
