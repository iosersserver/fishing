package net.seliba.fishing.utils.random;

import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

/**
 * A list of {@link RandomObject}s. Can select a random element based on their possibilities.
 *
 * @param <K> The type of the elements.
 */
public class RandomList<K extends RandomObject> {

  private final Map<K, IntegerRange> ENTRIES = new HashMap<>();
  private final Random RANDOM = ThreadLocalRandom.current();
  private int currentIndex = 0;

  /**
   * Adds an element to the list.
   *
   * @param entry The element which can later be selected randomly.
   */
  public void add(K entry) {
    assert entry.getPossibility() > 0;

    ENTRIES.put(entry, new IntegerRange(currentIndex, currentIndex + entry.getPossibility() - 1));
    currentIndex += entry.getPossibility();
  }

  /**
   * Returns whether or not the list has any elements.
   *
   * @return True if the list is empty.
   */
  public boolean isEmpty() {
    return ENTRIES.isEmpty();
  }

  /**
   * Generates a random element from the list based on their chances.
   *
   * @return The randomly selected element.
   */
  public K getRandomEntry() {
    int randomInt = RANDOM.nextInt(currentIndex);
    return ENTRIES.keySet()
      .stream()
      .filter(k -> ENTRIES.get(k)
        .isValueInside(randomInt))
      .findAny()
      .orElseThrow(NoSuchElementException::new);
  }

}
