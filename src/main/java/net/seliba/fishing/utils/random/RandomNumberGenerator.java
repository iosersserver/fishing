package net.seliba.fishing.utils.random;

import java.util.concurrent.ThreadLocalRandom;

/**
 * Class used for applying special attributes to some fishing loots.
 */
public class RandomNumberGenerator {

  /**
   * Returns whether or not a 1 in n event has happened.
   *
   * @param n The amount of possible outcomes.
   * @return True if the outcome has been a 0.
   */
  public static boolean hasHappened(int n) {
    return ThreadLocalRandom.current()
      .nextInt(n) == 0;
  }

}
