package net.seliba.fishing.utils.random;

/**
 * Represents a range of {@link Integer} with an upper and lower bound. Used for the {@link
 * RandomList}.
 */
public class IntegerRange {

  private final int lowerBound;
  private final int upperBound;

  /**
   * Represents a range of {@link Integer} with an upper and lower bound. The lower bound must be
   * smaller or equal to the upper bound.
   *
   * @param lowerBound The upper bound of this Integer range.
   * @param upperBound The lower bound of this Integer range.
   * @throws IllegalArgumentException Thrown if lowerBound > upperBound.
   */
  public IntegerRange(int lowerBound, int upperBound) throws IllegalArgumentException {
    if (lowerBound > upperBound) {
      throw new IllegalArgumentException("Lower bound should be smaller than upper bound!");
    }

    this.lowerBound = lowerBound;
    this.upperBound = upperBound;
  }

  /**
   * Returns whether or not the provided integer is inside the bounds of the range.
   *
   * @param otherInt The integer which will be checked.
   * @return Whether or not the integer is inside the range.
   */
  public boolean isValueInside(int otherInt) {
    return otherInt >= lowerBound && otherInt <= upperBound;
  }

}
