package net.seliba.fishing.utils.random;

/**
 * Represents an element which can be added to a {@link RandomList}. Default implementation of the
 * {@link RandomObject}, which should be suited for most if not all use cases.
 *
 * @param <K> Type of the object.
 * @see RandomList
 * @see RandomObject
 */
public class DefaultRandomObject<K> implements RandomObject {

  private final K value;
  private final int possibility;

  /**
   * Default implementation of an object which can be added to a {@link RandomList}
   *
   * @param value The value of the element.
   * @param possibility The possibility of the element. The actual possibility depends on the
   * possibilities of the other elements.
   */
  public DefaultRandomObject(K value, int possibility) {
    this.value = value;
    this.possibility = possibility;
  }

  /**
   * Returns the value hold in this object.
   *
   * @return The value hold in this object.
   */
  public K getValue() {
    return value;
  }

  /**
   * Returns the chance of this object being selected in the {@link RandomList}. The actual
   * possibility depends on the other possibilities in the list.
   *
   * @return The possibility of this element.
   */
  @Override
  public int getPossibility() {
    return possibility;
  }

}
