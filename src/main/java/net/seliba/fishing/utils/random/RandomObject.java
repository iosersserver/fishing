package net.seliba.fishing.utils.random;

/**
 * Represents an element which can be added to a {@link RandomList}.
 */
public interface RandomObject {

  /**
   * The possibility of the element being selected. The actual chance depends on the changes of the
   * other elements in the list.
   *
   * @return The possibility of this element being selected.
   */
  int getPossibility();

}
