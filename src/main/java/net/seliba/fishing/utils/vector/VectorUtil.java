package net.seliba.fishing.utils.vector;

import org.bukkit.Location;
import org.bukkit.util.Vector;

/**
 * Utils for Vector calculations.
 */
public class VectorUtil {

  /**
   * Utility method which returns the Vector from the currentLocation to the targetLocation. Used to
   * throw fished items to the Player.
   *
   * @param currentLocation The location where the vector should start
   * @param targetLocation The location where the vector should end
   * @return Normalized Vector from currentLocation to targetLocation
   */
  public static Vector getVectorBetweenLocations(Location currentLocation, Location targetLocation) {
    return targetLocation.toVector().subtract(currentLocation.toVector()).normalize();
  }

}
