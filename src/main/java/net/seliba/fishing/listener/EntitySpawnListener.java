package net.seliba.fishing.listener;

import net.seliba.fishing.scheduler.RodParticleScheduler;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.FishHook;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntitySpawnEvent;

/**
 * Event {@link Listener} used to add particles to fishing rods by registering them in the {@link
 * RodParticleScheduler}.
 */
public class EntitySpawnListener implements Listener {

  /**
   * Event listener used to display particles when a fishing rod is being cast.
   *
   * @param event The {@link EntitySpawnEvent} provided by Spigot.
   */
  @EventHandler
  public void onEntitySpawn(EntitySpawnEvent event) {
    if (event.getEntityType() != EntityType.FISHING_HOOK) {
      // We don't care about any entities that aren't a fishing hook
      return;
    }

    RodParticleScheduler.registerHook((FishHook) event.getEntity());
  }

}
