package net.seliba.fishing.listener;

import net.seliba.fishing.loot.LootGenerator;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerFishEvent;
import org.bukkit.event.player.PlayerFishEvent.State;
import org.bukkit.inventory.meta.Damageable;

/**
 * Event {@link Listener} used to generate random loot when a fishing rod is reeled in.
 */
public class PlayerFishListener implements Listener {

  /**
   * Event listener used to generate random loot when a fishing rod is reeled in.
   *
   * @param event The {@link PlayerFishEvent} provided by Spigot
   */
  @EventHandler
  public void onPlayerFish(PlayerFishEvent event) {
    if (event.getState() != State.CAUGHT_FISH) {
      // We don't care when no fish is caught
      return;
    }
    event.setCancelled(true);

    var fishHook = event.getHook();
    var hookLocation = fishHook.getLocation();
    var player = event.getPlayer();
    /*
    var item = hookLocation.getWorld().dropItem(hookLocation, new ItemStack(Material.BAKED_POTATO));
    item.setVelocity(VectorUtil.getVectorBetweenLocations(hookLocation, event.getPlayer().getLocation().add(0, 2, 0)));
     */
    // Generate and spawn the random loot
    var randomLoot = LootGenerator.generate();
    randomLoot.executeRandomLootGeneration(hookLocation, event.getPlayer());

    // Remove the fishing hook manually because we cancelled the event
    fishHook.remove();

    // Add damage to the fishing rod
    var inMainHand = player.getInventory()
      .getItemInMainHand()
      .getType() == Material.FISHING_ROD;
    var fishingRod = inMainHand ? player.getInventory()
      .getItemInMainHand() : player.getInventory()
      .getItemInOffHand();
    var fishingRodMeta = fishingRod.getItemMeta();
    var damageable = ((Damageable) fishingRodMeta);
    damageable.setDamage(damageable.getDamage() + 1);
    fishingRod.setItemMeta(fishingRodMeta);
    if (inMainHand) {
      player.getInventory().setItemInMainHand(fishingRod);
    } else {
      player.getInventory().setItemInOffHand(fishingRod);
    }
  }

}
